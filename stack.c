#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "stack.h"

#define INIT_MAGIC_NUM 0xDEADBEEF

void stack_array_init(stack_info_t* stack,uint8_t *array,uint8_t size)
{
	if(!stack || !array)
		return;

	if(stack->init_magic_number == INIT_MAGIC_NUM)
		return;

	stack->array = array;
	stack->size = size;
	stack->length = 0;
	stack->top_index = -1;
	stack->init_magic_number = INIT_MAGIC_NUM;

}

bool stack_array_push(stack_info_t* stack,uint8_t data)
{
	if(stack == NULL)
		return false;

	if(stack->init_magic_number != INIT_MAGIC_NUM)
		return false;

	if(stack->length >= stack->size)
		return false;

	stack->top_index++;
	stack->array[stack->top_index] = data;
	stack->length++;

	return true;
}

bool stack_array_pop(stack_info_t* stack,uint8_t* data)
{
	if(stack == NULL)
		return false;

	if(data == NULL)
		return false;

	if(stack->init_magic_number != INIT_MAGIC_NUM)
		return false;

	if(stack->length == 0)
		return false;

	*data = stack->array[stack->top_index];
	stack->array[stack->top_index] = 0;
	stack->top_index--;
	stack->length--;

	return true;
}

void stack_array_print_details(stack_info_t* stack)
{
	if(stack == NULL)
		return;

	if(stack->init_magic_number != INIT_MAGIC_NUM)
		return;

	printf("\n");
	printf("length:%d(size:%d)\n",stack->length,stack->size);

	for(uint16_t i=0; i < stack->size; i++)
		printf(" %d",stack->array[i]);
	printf("\n");
}
