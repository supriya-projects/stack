#include <stdio.h>
#include <stdint.h>
#include "stack.h"

#define ARRAY_SIZE 10

static uint8_t __array[ARRAY_SIZE];
static stack_info_t __stack;

static void __add_data(uint8_t data);
static void __remove_data(void);

int main(void)
{
	stack_array_init(&__stack, __array, ARRAY_SIZE);
	stack_array_print_details(&__stack);

	__add_data(10);
	__add_data(20);
	__add_data(30);
	__add_data(40);
	__add_data(50);
	stack_array_print_details(&__stack);

	__remove_data();
	__remove_data();
	stack_array_print_details(&__stack);

	return 0;

}

static void __add_data(uint8_t data)
{
	bool result;

	result = stack_array_push(&__stack, data);
	if(result == false)
		printf("failed to add data\n");
	else
		printf("adding:%d\n",data);
}

static void __remove_data(void)
{
	uint8_t data;
	bool result;

	result = stack_array_pop(&__stack, &data);
	if(result == false)
		printf("failed to remove data\n");
}

