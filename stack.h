#ifndef STACK_H_
#define STACK_H_

#include <stdbool.h>
#include <stdint.h>

typedef struct __stack_info_ {
	uint8_t* array;
	int top_index;
	uint8_t size;
	uint8_t length;
	uint32_t init_magic_number;
}stack_info_t;

void stack_array_init(stack_info_t* stack,uint8_t *array,uint8_t size);
bool stack_array_push(stack_info_t* stack,uint8_t data);
bool stack_array_pop(stack_info_t* stack,uint8_t* data);
void stack_array_print_details(stack_info_t* stack);

#endif /* STACK_H_ */
